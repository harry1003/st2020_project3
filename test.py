# -*- coding: utf-8 -*
import os  
import time

# [Content] XML Footer Text
def test_hasHomePageNode():
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('首頁') != -1

# [Behavior] Tap the coordinate on the screen
def test_tapSidebar():
	os.system('adb shell input tap 100 100')

# 1. [Content] Side Bar Text
def test_hasSideBarText():
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('查看商品分類') != -1
	assert xmlString.find('查訂單') != -1
	assert xmlString.find('追蹤') != -1
	assert xmlString.find('智慧標籤') != -1
	assert xmlString.find('PChome') != -1
	assert xmlString.find('線上手機回收') != -1

# 2. [Screenshot] Side Bar Text
def test_hasSideBarScreen():
	os.system('adb shell screencap -p /sdcard/2_sideBar.png')
	os.system('adb pull /sdcard/2_sideBar.png')
	os.system('adb shell input tap 1000 100')
	time.sleep(1)
	
# 3. [Context] Categories
def test_hasCategories1Text():
	os.system('adb shell input swipe 540 1300 540 850 100')
	time.sleep(1)
	os.system('adb shell input tap 950 300')
	time.sleep(1)

	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('精選') != -1
	assert xmlString.find('3C') != -1
	assert xmlString.find('周邊') != -1
	assert xmlString.find('NB') != -1

# 4. [Screenshot] Categories
def test_hasCategories1Screen():
	os.system('adb shell screencap -p /sdcard/4_categories1.png')
	os.system('adb pull /sdcard/4_categories1.png')
	os.system('adb shell input tap 950 300')
	time.sleep(1)

# # 5. [Context] Categories page
def test_hasCategories2Text():
	os.system('adb shell input tap 300 1700')
	time.sleep(1)

	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('24H購物') != -1
	assert xmlString.find('購物中心') != -1
	assert xmlString.find('3C') != -1
	assert xmlString.find('周邊') != -1
	assert xmlString.find('NB') != -1
	assert xmlString.find('運動戶外') != -1

# # 6. [Screenshot] Categories page
def test_hasCategories2Screen():
	os.system('adb shell screencap -p /sdcard/6_categories2.png')
	os.system('adb pull /sdcard/6_categories2.png')
	os.system('adb shell input tap 150 1700')
	time.sleep(1)

# 7. [Behavior] Search item “switch”
def test_searchSwitchBehavior():
	os.system('adb shell input tap 950 100')
	time.sleep(1)
	os.system('adb shell input text "switch"')
	time.sleep(1)
	os.system('adb shell input keyevent "KEYCODE_ENTER"')
	time.sleep(3)

# 8. [Behavior] Follow an item and it should be add to the list
def test_followItemBehavior():
	os.system('adb shell input tap 900 500')  #進入商品
	time.sleep(3)
	os.system('adb shell input tap 150 1700') #追蹤
	time.sleep(1)
	os.system('adb shell input keyevent "KEYCODE_BACK"')
	time.sleep(1)
	os.system('adb shell input tap 150 1700')   #回首頁
	time.sleep(1)
	os.system('adb shell input tap 100 100') #開選單
	time.sleep(3)
	os.system('adb shell input tap 100 900') #進入
	time.sleep(3)

# 9. [Behavior] Navigate tto the detail of item
def test_showDetailBehavior():
	os.system('adb shell input tap 100 900') #點商品
	time.sleep(2)
	os.system('adb shell input swipe 540 1300 540 850 100') #下滑
	time.sleep(1)
	os.system('adb shell input tap 500 100') # 點detail
	time.sleep(2)

# 10. [Screenshot] Disconnetion Screen
def test_dieconnectionScreen():
	os.system('adb shell svc wifi disable')
	os.system('adb shell svc data disable')
	time.sleep(1)
	os.system('adb shell input keyevent "KEYCODE_BACK"')
	time.sleep(1)
	os.system('adb shell input keyevent "KEYCODE_BACK"')
	time.sleep(1)
	os.system('adb shell screencap -p /sdcard/10_dieconnection.png')
	os.system('adb pull /sdcard/10_dieconnection.png')

